# Slax Linux Installer 

Slax is a compact, fast, and modern Linux operating system that combines sleek design with modular approach. With the ability to run directly from a USB flash drive without the need for installation, Slax is truly portable and fits easily in your pocket. Despite its small size, it offers visually appealing graphical user interface and a carefully curated selection of basic pre-installed programs such as a file manager, text editor, terminal, and more.

## Installation with script 
1. Specify you'r usb device and iso image on configuration file:
```python
# device 
target_device='/dev/sdb';
target_partition='/dev/sdb1';

# system image [read only]
installation_source='/dev/sr0';

# mount point 
directory=/media;
isoImage=/media/iso;
usbDevice=/media/usb;
```

For `slax-64bit-debian-12.2.0.iso` file then mention it like:
```python
installation_source='/media/sdb/slax-64bit-debian-12.2.0.iso';
```

2. Run the installer script with root permission: 
```bash
bash ./install
```

## Manual Installation 
1. Mount iso image and usb device. 
2. Copy `/slax` directory into usb device. 
3. Run `bootinst.sh` from your usb device which file can be found into `/slax/boot/bootinst.sh`. 
4. Reboot 

# স্ল্যাক্স লিনাক্স
স্ল্যাক্স লিনাক্স (Slax Linux) হলো খুবই লাইট এবং পোর্টেবল লিনাক্স ভিত্তিক অপারেটিং সিস্টেম। প্রাথমিকভাবে এটি লাইভ সিডি বা ইউএসবি পেন ড্রাইভ থেকে চালানোর জন্য ডিজাইন করা হয়েছে। এটি কম্পিউটারে ইনস্টল না করেই সরাসরি ব্যবহার করা যায়। অর্থাৎ এটি সর্বাবস্থায় রিড অনলি মোডে থাকবে। তবে প্রয়োজন অনুসারে রাইট করা সম্ভব।

![](images/Laptop-with-on-screen.png)

অনেকেই নিজের পেন ড্রাইভে লিনাক্স ডিস্ট্রিবিউশন ইন্সটল করে রাখতে চায়। বুট করা থাকলে প্রয়োজনে অপ্রয়োজনে এসব বেশ কাজে আসে। হুট করে অপারেটিং সিস্টেম ক্র্যাশ করলে বা কোন কিছু টেস্ট করলে ইন্সটল না করেই পেন ড্রাইভ থেকেই কাজগুলো অল্প পরিসরে সেরে ফেলা যায়।

স্ল্যাক্স লিনাক্স ‘স্ল্যাকওয়্যার’ এবং ‘ডেবিয়ান’ দুইটি এডিশনে পাওয়া যায়। এটিতে ফ্লাক্সবক্স উইন্ডো ম্যানেজার দেয়া রয়েছে। এছাড়া খুবই সাধারন কিছু এপ্লিকেশন প্রি-ইন্সটল করা রয়েছে। তবে আপনি চাইলে প্রয়োজনমতো গ্রাফিক্যাল ভিত্তিক এবং কমান্ড লাইন ভিত্তিক বিভিন্ন টুলস ইন্সটল করে নিতে পারেন।

### স্ল্যাক্স লিনাক্সের কিছু বৈশিষ্ট্যঃ

হালকা এবং পোর্টেবলঃ স্ল্যাক্স খুবই ছোট এবং হালকা অপারেটিং সিস্টেম, যা সহজেই একটি ইউএসবি ড্রাইভে ইন্সটল করা যায়। কারন খবই কম স্পেস খরচ করে। লাইট ডেস্কটপ থাকার কারনে এর র‍্যাম ইউজেজও অনেক কম।
মডুলার আর্কিটেকচারঃ স্ল্যাক্সের মডুলার ডিজাইন ইউজারকে বিভিন্ন মডিউল যোগ করা অথবা বাদ দেয়ার সুযোগ দেয়।
লাইভ অপারেটিং সিস্টেমঃ এটি প্রাথমিকভাবে লাইভ সিস্টেম হিসেবে কাজ করে, অর্থাৎ এটি সরাসরি র‌্যাম থেকে রান করা যায়। সিস্টেমের হার্ড ড্রাইভে ইনস্টল করার প্রয়োজন নেই।
উইন্ডো ম্যানেজারঃ স্ল্যাক্স সাধারণত লাইটওয়েট উইন্ডো ম্যানেজার ব্যবহার করে, যেমন Fluxbox বা Openbox, যা দ্রুত এবং লাইট ডেস্কটপ এনভাইরনমেন্ট/উইন্ডো ম্যানেজার।

## পেন ড্রাইভে ইন্সটলেশন প্রক্রিয়া
ইন্সটলেশন প্রকৃয়া সহজ করার জন্য আমি একটি ব্যাশ স্ক্রিপ্ট তৈরি করেছি। তবে ব্যাশ স্ক্রিপ্ট ছাড়াও এটি ম্যানুয়ালি ইন্সটল করা যায়। আমি এখানে দুটো পদ্ধতিই আলোচনা করবো। আর হ্যা, ইন্সটলের জন্য অবশ্যই সিস্টেমে অন্য একটি লিনাক্স ডিস্ট্রিবিউশন ইন্সটল করা থাকতে হবে।

### ব্যাশ স্ক্রিপ্টের মাধ্যমে ইন্সটলেশন
প্রথমে গিট রিপোজিটারি থেকে ক্লোন করতে হবেঃ

git clone https://gitlab.com/farhansadik/slax-linux-installer.git
এরপর configuration.cfg ফাইলে মডিফাই করতে হবে। ফাইলটি ওপেন করলে আমরা নীচের মতো দেখতে পাওয়া যাবেঃ
```python
# device 
target_device='/dev/sdb';
target_partition='/dev/sdb1';

# system image [read only]
installation_source='/dev/sr0';

# mount point 
directory=/media;
isoImage=/media/iso;
usbDevice=/media/usb;
```

এখানে `target_device` ভ্যারিয়েবলে পেন ড্রাইভের এড্রেস লিখতে হবে এবং `target_partition` এ পার্টিশনের এড্রেস দিতে হবে। ডিভাইস লিস্ট জানতে `lsblk` কমান্ড ব্যবহার করতে হবে।

`installation_source` অংশে মাউন্টকৃত সিস্টেম ইমেজের এড্রেস দিতে হবে। সরাসরি আইসো ফাইলের লোকেশন দিতে চাইলেঃ

```python
installation_source='/media/sdb/slax-64bit-debian-12.2.0.iso';
```

মাউন্ট পয়েন্ট অংশে পরিবর্তন করার বিশেষ কোন প্রয়োজন নেই। সবকিছু ঠিকঠাক থাকলে এবার ইন্সটলেশন স্ক্রিপ্ট রান করতে হবেঃ

```bash
bash ./install
```

ইন্সটলেশন শেষে রিবুট করে পেন ড্রাইভ থেকে সিস্টেম বুট করতে হবে।

> খেয়াল রাখতে হবে, স্ল্যাক্স লিনাক্স মাল্টিবুট এবং UEFI সাপোর্ট করে না।

### ম্যানুয়াল ইন্সটলেশন
স্ক্রিপ্টের উপর ভরসা না থাকলে, ম্যানুয়ালি নিজে নিজে ইন্সটল করতে পারেন। এর জন্য প্রথমে সিস্টেম ইমেজ (আইসো) ফাইলটি ডাউনলোড করুন। এর পর পেন ড্রাইভ সিস্টেমে ইন্সার্ট করে ফর্ম্যাট করুন — MBR পার্টিশন টেবিলে এবং পার্টিশনটি FAT32 তে। এরপর

1. সিস্টেম ইমেজ (আইসো) ফাইল এবং পেন ড্রাইভ মাউন্ট করুন।
2. আইসো’র ভেতরে থাকা `/slax` ডিরেক্টরিটি কপি করে পেন ড্রাইভে পেস্ট করুন।
3. এরপর পেন ড্রাইভের ভেতরে `bootinst.sh` ফাইলটি এক্সিকিউট করুন। ফাইলটি `/slax/boot/bootinst.sh` লোকেশনে পেয়ে যাবেন।
4. এরপর রিবুট করে পেন ড্রাইভ থেকে সিস্টেম বুট করুন।

![](images/desktop.PNG)

## পার্মানেন্ট রাইট করার পদ্ধতি
যেহেতু আমরা জানি স্ল্যাক্স লিনাক্স রিড অনলি মোডে রান হয়। আমরা কোন কিছু যদি পার্মানেন্ট পরিবর্তন করতেই চাই, তাহলে বুট মেনু থেকে প্রথমে start a new session অপশন থেকে বুট করতে হবে।

![](images/boot_menu.PNG)

এরপর নীচের মতো কমান্ড দিয়ে একটি নতুন মডিউল তৈরি করতে হবেঃ

```bash
savechanges /root/ching_chang.sb
```

কাজের উপর নির্ভর করে এটি জায়গা নিয়ে থাকে। খেয়াল রাখতে হবে পেন ড্রাইভে যেন পর্যাপ্ত জায়গা থাকে। এরপর মডিউলটি পেন ড্রাইভের `/slax/modules/` ডিরেক্টরিতে পেস্ট করতে হবে। এরপর সিস্টেম রিবুট করতে হবে।

#### Source 
1. https://www.slax.org/starting.php
2. https://www.slax.org/customize.php
3. https://medium.com/@rkfarhansadik/%E0%A6%B8%E0%A7%8D%E0%A6%B2%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%95%E0%A7%8D%E0%A6%B8-%E0%A6%B2%E0%A6%BF%E0%A6%A8%E0%A6%BE%E0%A6%95%E0%A7%8D%E0%A6%B8-f33fab98218e


Farhan Sadik